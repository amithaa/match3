using UnityEngine;

public class Grid
{
    private int width;
    private int height;
    private int cellSize;

    private float offsetX;
    private float offsetY;

    public Grid(int width, int height, int cellSize, float offsetX, float offsetY)
    {
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }

    public int GetWidth() => this.width;
    public int GetHeight() => this.height;

    public Vector3 GetTilePosition(int x, int y)
    {
        return new Vector3(x * cellSize + offsetX, y * cellSize + offsetY, 0);
    }
}

using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Collections;
using System;

public class Match3 : MonoBehaviour
{
    private Grid grid;

    public int width;
    public int height;
    public int cellSize;

    public float offsetX;
    public float offsetY;

    public GameObject[] tilePrefabArray;
    private Tile[,] tiles;

    public Transform parent;

    public event EventHandler OnRearrangeComplete;

    private void Awake()
    {
        grid = new Grid(width, height, cellSize, offsetX, offsetY);
        tiles = new Tile[width, height];

        CreateTileGrid();
    }

    public void DestroyTileAndCheckForEmpty(Tile tile)
    {
        Vector2Int coordinates = tile.GetCoordinates();
        Destroy(tiles[coordinates.x, coordinates.y].gameObject);
        tiles[coordinates.x, coordinates.y] = null;

        StartCoroutine(FindEmptyTilesAndMatches());
    }

    private void CreateTileGrid()
    {
        List<GameObject> possibleTileList = new List<GameObject>();

        for (int x = 0, width = grid.GetWidth(); x < width; ++x) {
            for (int y = 0, height = grid.GetHeight(); y < height; ++y) {
                //Check if the same tile exists on both x - 1st, x - 2th column, 
                //if yes don't add to list of tile possibilities
                if (x > 1) {
                    for (int w = 0, tilePrefabArrLen = tilePrefabArray.Length; w < tilePrefabArrLen; ++w) {
                        Tile tilePrefab = tilePrefabArray[w].GetComponent<Tile>();
                        if (!(tiles[x - 1, y].Id == tilePrefab.Id &&
                            tiles[x - 2, y].Id == tilePrefab.Id)) {
                            possibleTileList.Add(tilePrefabArray[w]);
                        }
                    }
                }
                else {
                    possibleTileList = tilePrefabArray.ToList();
                }

                GameObject newTile = Instantiate(possibleTileList[UnityEngine.Random.Range(0, possibleTileList.Count)],
                    grid.GetTilePosition(x, y), Quaternion.identity, parent);

                tiles[x, y] = newTile.GetComponent<Tile>();
                tiles[x, y].SetCoordinates(x, y);

                possibleTileList.Clear();
            }
        }
    }

    private IEnumerator FindEmptyTilesAndMatches()
    {
        while (true) {
            for (int x = 0, width = grid.GetWidth(); x < width; ++x) {
                for (int y = 0, height = grid.GetHeight(); y < height; ++y) {
                    if ((bool)IsGridPositionEmpty(x, y)) {
                        MoveTiles(x, y);
                    }
                }
            }
            yield return new WaitForSeconds(0.8f);

            List<Vector2Int> matchedTiles = CheckForMatches();

            if (matchedTiles.Count > 0) {
                yield return new WaitForSeconds(0.3f);

                ClearMatches(matchedTiles);
            }
            else {
                break;
            }
        }

        yield return new WaitForSeconds(0.03f);

        OnRearrangeComplete?.Invoke(this, EventArgs.Empty);
    }

    private void MoveTiles(int x, int y)
    {
        int oldYIndex = y;
        for (int filler = y, height = grid.GetHeight() - 1; filler < height; ++filler) {
            if (!(bool)IsGridPositionEmpty(x, filler + 1)) {
                Vector3 currentPosition = grid.GetTilePosition(x, oldYIndex);

                Tile temp = tiles[x, filler + 1];
                tiles[x, oldYIndex] = temp;
                tiles[x, oldYIndex].SetCoordinates(x, oldYIndex);
                tiles[x, filler + 1] = null;

                tiles[x, oldYIndex].MoveToPosition(currentPosition);

                oldYIndex = filler + 1;
            }
        }
    }

    private List<Vector2Int> CheckForMatches()
    {
        List<Vector2Int> matchedTiles = new List<Vector2Int>();

        for (int x = 0, width = grid.GetWidth(); x < width; ++x) {
            for (int y = 0, height = grid.GetHeight(); y < height; ++y) {
                if (!(bool)IsGridPositionEmpty(x, y)) {
                    Tile tile = tiles[x, y];

                    //Can be extended for vertical matches as well
                    List<Vector2Int> horizontalMatches = FindHorizontalMatchesForTile(tile);

                    if (horizontalMatches.Count >= 2) {
                        matchedTiles.Add(tile.GetCoordinates());
                        matchedTiles.AddRange(horizontalMatches);
                    }
                }
            }
        }

        return matchedTiles;
    }

    private void ClearMatches(List<Vector2Int> matchedTiles)
    {
        for (int i = 0, matchedCount = matchedTiles.Count; i < matchedCount; ++i) {
            Vector2Int toRemove = (Vector2Int)matchedTiles[i];
            Destroy(tiles[toRemove.x, toRemove.y]?.gameObject);
            tiles[toRemove.x, toRemove.y] = null;
        }
    }

    private List<Vector2Int> FindHorizontalMatchesForTile(Tile currentTile)
    {
        List<Vector2Int> result = new List<Vector2Int>();

        int x = currentTile.GetCoordinates().x;
        int y = currentTile.GetCoordinates().y;

        //Check for matches along its row
        for (int i = x + 1, width = grid.GetWidth(); i < width; i++) {
            if (!(bool)IsGridPositionEmpty(i, y)) {
                Tile nextTile = tiles[i, y];
                int nextTileId = nextTile.Id;
                if (nextTileId != currentTile.Id) {
                    break;
                }
                result.Add(nextTile.GetCoordinates());
            }
            else {
                break;
            }
        }
        return result;
    }

    private bool? IsGridPositionEmpty(int x, int y)
    {
        return tiles[x, y] == null ? true : false;
    }
}

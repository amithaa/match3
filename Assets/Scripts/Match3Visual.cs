using System;
using UnityEngine;

public class Match3Visual : MonoBehaviour
{
    [SerializeField]
    private Match3 match3;

    [SerializeField]
    bool isReady = true;

    void OnEnable()
    {
        match3.OnRearrangeComplete += SetInputReady;
    }

    void OnDisable()
    {
        match3.OnRearrangeComplete -= SetInputReady;
    }

    void Update()
    {
        if (isReady) {
            if (Input.GetMouseButton(0)) {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit)) {
                    Tile tile = hit.transform.gameObject.GetComponent<Tile>() ?? null;
                    if (tile != null) {
                        isReady = false;

                        match3.DestroyTileAndCheckForEmpty(tile);
                    }
                }
            }
        }
    }

    private void SetInputReady(object sender, EventArgs e)
    {
        isReady = true;
    }
}

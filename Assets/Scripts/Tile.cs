using System.Collections;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [SerializeField]
    private int id;

    private int xCoord;
    private int yCoord;

    public int Id
    {
        get { return id; }
        set { id = value; }
    }

    public void MoveToPosition(Vector3 position)
    {
        StartCoroutine(MoveTileSmoothly(position));
    }

    private IEnumerator MoveTileSmoothly(Vector3 finalPosition)
    {
        float t = 0f;
        while (t < 1f) {
            t += Time.deltaTime;
            transform.position = Vector3.Lerp(gameObject.transform.localPosition, finalPosition, Mathf.SmoothStep(0f, 1f, t));
            yield return null;
        }
    }

    public void SetCoordinates(int x, int y)
    {
        xCoord = x;
        yCoord = y;
    }

    public Vector2Int GetCoordinates()
    {
        return new Vector2Int(xCoord, yCoord);
    }
}
